#ifndef SPIKE_H
#define SPIKE_H

#include "entity.h"

struct Spike : public Entity {
	Spike(Game *ngame, sf::Vector2f nposition);
	void update();
	void render(sf::RenderWindow &window);
};
#endif

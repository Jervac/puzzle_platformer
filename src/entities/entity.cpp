#include "entity.h"
#include "../game.h"
#include "../utils.h"
#include "../globals.h"
#include "math.h"

Entity::Entity(Game *ngame, sf::Vector2f nposition, sf::Vector2f nsize) {
	game = ngame;
	position = nposition;
	size = nsize;
}

Entity::Entity(Game *ngame, sf::Vector2f nposition) {
	game = ngame;
	position = nposition;
	size = sf::Vector2f(TILE_SIZE, TILE_SIZE);
}

Entity::Entity(Game *ngame) {
	game = ngame;
	size = sf::Vector2f(TILE_SIZE, TILE_SIZE);
}

Rect Entity::bounds() {
	return Rect(position.x, position.y, size.x, size.y);
}

void Entity::updateGravity() {

	// FALLING
	if (!jumping) {
		if (fall_velocity < fall_velocity_max) {
			fall_velocity += fall_acceleration;
		}

		// Incremental collision checks so entity doesn't go inside objects
		for (int i = 0; i<fall_velocity; i++) {
			if (up_side) position.y++;
			else position.y--;

			// Platform collision under player
			can_jump = false;
			standing_on_flipper = false;
			for (auto e : game->entities) {
				if (up_side) {
					if (e != this &&
						((e->solid && e->type != "player") || (this->solid && e->type == "player")) && // NOTE: Right side of this allows sand blocks to fall on player head
						e->bounds().intersects(bounds()) &&
						e->position.y + e->size.y > position.y + size.y) {

						// Jumping on head of an enemy if
						if (e->hostile && !jumping) {
							jump();
							jump_velocity = head_bounce_velocity;
						}
						else {
							position.y = e->position.y - size.y;
							can_jump = true;
							fall_velocity = 0;
							if (e->type == "flipper") standing_on_flipper = true;
						}
					}
				}
				else {
					if (e != this &&
						((e->solid && e->type != "player") || (this->solid && e->type == "player")) && // NOTE: Right side of this allows sand blocks to fall on player head
						e->bounds().intersects(bounds()) &&
						e->position.y < position.y) {

						// Jumping on head of an enemy
						if (e->hostile && !jumping) {
							jump();
						}
						else {
							position.y = e->position.y + e->size.y;
							can_jump = true;
							fall_velocity = 0;
							if (e->type == "flipper") standing_on_flipper = true;
						}
					}
				}
			}
		}

	}
}

// TODO: Render sprite a little lower when on flippers
void Entity::flip(bool nup_side) {
	if (can_jump && standing_on_flipper) {
		bool can_flip = true;

		if (up_side) {
			position.y += (size.y + TILE_SIZE);
			for (auto e : game->entities) {
				if (e != this && e->bounds().intersects(bounds())) {
					can_flip = false;
					position.y -= (size.y + TILE_SIZE);
				}
			}
		}
		else {
			position.y -= (size.y + TILE_SIZE);
			for (auto e : game->entities) {
				if (e != this && e->bounds().intersects(bounds())) {
					can_flip = false;
					position.y += (size.y + TILE_SIZE);
				}
			}
		}

		if (can_flip) {
			up_side = nup_side;
			// Flip all enemies on same flipper as player
			if (type == "player") {
				Rect under_bounds(0, 0, 0, 0); // Bounds underneath the player
				Rect total_flipper(0, 0, TILE_SIZE, TILE_SIZE); // Sum of flipper tiles under player to form one whole

																// Position the under_bounds
				if (this->up_side) under_bounds = Rect(this->position.x, this->position.y + this->size.y, TILE_SIZE, TILE_SIZE);
				else under_bounds = Rect(this->position.x, this->position.y - TILE_SIZE, TILE_SIZE, TILE_SIZE);

				bool found_starting_flipper = false; // Found flipper directly under player to start with?
				for (auto f : game->entities) {
					// Find the flipper directly under the player and start with that
					if (f->type == "flipper" && f->bounds().intersects(under_bounds)) {
						total_flipper.setPosition(f->position);
						found_starting_flipper = true;
						break;
					}
				}

				if (found_starting_flipper) {

					// Move total_flipper x left to fit the total
					while (true) {
						// Shift 1 unit to the left
						total_flipper.x -= TILE_SIZE;

						// See if there's a flipper here
						bool found = false;
						for (auto ft : game->entities) {
							if (ft->type == "flipper" && ft->position.x == total_flipper.x && ft->position.y == total_flipper.y) {
								found = true;
								break;
							}
						}

						// Move back a unit if oversized
						if (!found) {
							total_flipper.x += TILE_SIZE;
							break;
						}
					}

					// Increase total_flipper width to the right to fit the total
					while (true) {
						// Target position total_flipper wants to stretch to
						Rect temp_target(total_flipper.x + total_flipper.width + TILE_SIZE, total_flipper.y, TILE_SIZE, TILE_SIZE);

						// See if target would still part of total
						bool found = false;
						for (auto ft : game->entities) {
							if (ft->type == "flipper" && ft->bounds().intersects(temp_target)) {
								total_flipper.width += TILE_SIZE;
								found = true;
								break;
							}
						}

						// Move back a unit if oversized
						if (!found) break;
					}

					// Flip all walkers standing on the flipper
					for (auto w : game->entities) {
						// total_flipper with an extra height of TILE_SIZE to see if walker is close enough to be considered "on" the flipper
						Rect flipper_radius(total_flipper.x, total_flipper.y - TILE_SIZE, total_flipper.width, total_flipper.height + (TILE_SIZE * 2));

						if (w->type == "walker" && w->bounds().intersects(flipper_radius)) {
							w->flip(!w->up_side);
							break;
						}
					}
				}
			}
		}
	}
}

void Entity::jump() {
	jumping = true;
	jump_velocity = jump_velocity_max;
	can_jump = false;
	fall_velocity = 0;
}

void Entity::jump(float specified_velocity) {
	jumping = true;
	jump_velocity = specified_velocity;
	can_jump = false;
	fall_velocity = 0;
}

sf::Vector2f Entity::getTilePos() {
	return sf::Vector2f(round(position.x / TILE_SIZE), round(position.y / TILE_SIZE));
}
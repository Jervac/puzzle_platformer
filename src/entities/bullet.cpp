#include "bullet.h"
#include "../game.h"
#include "../utils.h"

Bullet::Bullet(Game *ngame, sf::Vector2f nposition, bool nleft_right)
: Entity(ngame, nposition) {
    type = "bullet";
    hostile = true;
    solid = false;
    left_right = nleft_right;
    x_velocity = 12;
    size.x = TILE_SIZE/2;
    size.y = TILE_SIZE/2;
}

void Bullet::update() {
    if (left_right) position.x += x_velocity;
    else position.x -= x_velocity;
    
    for (auto e : game->entities) {
        // Kill player
        if (e->type == "player" && bounds().intersects(e->bounds())) {
            e->alive = false;
            alive = false;
            break;
        }
        // Die on collision with solids
        else if (e != this && (e->solid || e->hostile) && bounds().intersects(e->bounds())) {
            alive = false;
        }
    }
}

void Bullet::render(sf::RenderWindow &window) {
    sf::RectangleShape shape;
    shape.setPosition(position);
    shape.setSize(size);
    if (left_right) shape.setFillColor(sf::Color::Red);
    else shape.setFillColor(sf::Color::Magenta);
    window.draw(shape);
}

#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include "../globals.h"

struct Rect;
struct Game;

struct Entity {
	Entity(Game *ngame, sf::Vector2f nposition, sf::Vector2f nsize);
	Entity(Game *ngame, sf::Vector2f nposition);
	Entity(Game *ngame);

	Game* game;
	std::string type = "entity";

	sf::Vector2f position = sf::Vector2f(0, 0);
	sf::Vector2f direction = sf::Vector2f(0, 1);
	sf::Vector2f size = sf::Vector2f(0, 0);
	sf::Sprite sprite;
	sf::Texture texture;

	bool jumping = false;
	bool alive = true;
	bool solid = false;
	bool hostile = false;
	bool standing_on_flipper = false;
	bool up_side = true;
	bool can_jump = false;// TODO: change this to on_ground
	bool left_right = true; // true when right false when left

							// Default velocity if not using the others
	float velocity = 4;

	// x axis movement
	float x_velocity = 0;
	float x_velocity_max = 9.9f;
	float x_velocity_max_default = x_velocity_max;
	float x_acceleration = .6f;
	float x_acceleration_air = .1f;

	// jumping
	float jump_velocity = 0;
	float jump_velocity_max = 18.5f;
	float jump_velocity_min = 12.6f; // min velocity before you can start falling
	float jump_velocity_min_end = jump_velocity_min - 5.5f; // end range of min
	float jump_acceleration = .64f; // Acceleration towards peak
	float head_bounce_velocity = jump_velocity_max * 1.1f;

	bool bounced_on_enemy_head = false;

	// falling
	float fall_velocity = 0;
	float fall_velocity_max = 18;
	float fall_acceleration = .9f;

	virtual void update() = 0;
	virtual void render(sf::RenderWindow& window) = 0;
	void flip(bool nup_side);
	void updateGravity();
	void jump();
	void jump(float head_bounce_velocity);
	sf::Vector2f getTilePos();
	Rect bounds();
};
#endif

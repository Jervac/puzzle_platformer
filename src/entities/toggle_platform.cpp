 #include "toggle_platform.h"
#include "../game.h"
 
 TogglePlatform::TogglePlatform(Game *ngame, sf::Vector2f nposition, std::string nname_tag, bool non)
     : Platform(ngame, nposition) {
     type = "toggle_platform";
     name_tag = nname_tag;
     on = non;
 }
 
 void TogglePlatform::update() {
     solid = on;
 }
 
 void TogglePlatform::render(sf::RenderWindow &window) {
     sf::RectangleShape shape;
     shape.setPosition(position);
     shape.setSize(size);
     if (on) {
         shape.setFillColor(sf::Color::Magenta);
     } else {
         shape.setFillColor(sf::Color(0, 0, 0, 0));
         shape.setOutlineThickness(2);
     }
     window.draw(shape);
}

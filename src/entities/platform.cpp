#include "platform.h"

Platform::Platform(Game *ngame, sf::Vector2f nposition) 
: Entity(ngame, nposition) {
    type = "platform";
    solid = true;
}

void Platform::update() {}

void Platform::render(sf::RenderWindow &window) {
    sf::RectangleShape shape;
    shape.setPosition(position);
    shape.setSize(size);
    shape.setFillColor(sf::Color::Black);
    window.draw(shape);
}

#include "player.h"
#include "../utils.h"
#include "../game.h"
#include "../globals.h"
#include "door.h"
#include "chunk.h"

Player::Player(Game *ngame)
	: Entity(ngame) {
	type = "player";
	solid = false;
	position.x = 100;
	position.y = 300;
	size.x = 81;
	size.y = 127;

	texture.loadFromFile("data/tim0.png");
	sprite.setTexture(texture);
	sprite.setPosition(position);
	sprite.setScale(size.x / sprite.getLocalBounds().width, size.y / sprite.getLocalBounds().height);
	sprite.setScale({ 1, 1 });
	sprite.setOrigin({ 0, 0 });
}

void Player::update() {
	// FALLING
	if (!jumping) {
		bounced_on_enemy_head = false;

		if (fall_velocity < fall_velocity_max) {
			fall_velocity += fall_acceleration;
		}

		// NOTE(Jervac): The for loop makes collision checked in increments so player doesn't go inside
		// any solid objects!
		for (int i = 0; i<fall_velocity; i++) {
			if (up_side) position.y++;
			else position.y--;

			// Platform collision under player
			can_jump = false;
			standing_on_flipper = false;
			for (auto e : getCurrentChunk()->entities) {
				if (up_side) {
					if (e != this && e->solid &&e->bounds().intersects(bounds()) && e->position.y + e->size.y > position.y + size.y) {

						// Jumping on head of an enemy
						if (e->hostile && !jumping) {
							jump(head_bounce_velocity);
							bounced_on_enemy_head = true;
						}
						// Landing on solid
						else {
							position.y = e->position.y - size.y;
							can_jump = true;
							fall_velocity = 0;
							if (e->type == "flipper") standing_on_flipper = true;
						}
					}
				}
				else {
					if (e != this && e->solid && e->bounds().intersects(bounds()) && e->position.y < position.y) {

						// Jumping on head of an enemy
						if (e->hostile && !jumping) {
							jump(head_bounce_velocity);
							bounced_on_enemy_head = true;
						}
						// Landing on solid
						else {
							position.y = e->position.y + e->size.y;
							can_jump = true;
							fall_velocity = 0;
							if (e->type == "flipper") standing_on_flipper = true;
						}
					}
				}
			}
		}
	}

	// Start falling at peak of jump
	if (jumping && jump_velocity <= 0) jumping = false;

	// JUMPING
	if (jumping) {
		if (up_side) {
			position.y -= jump_velocity;
		}
		else {
			position.y += jump_velocity;
		}

		jump_velocity -= jump_acceleration;


		// platform collision above player
		for (auto e : getCurrentChunk()->entities) {
			if (up_side) {
				if (e != this && e->solid && e->bounds().intersects(bounds()) && e->position.y < position.y) {
					position.y += jump_velocity;
					jumping = false;
					jump_velocity = 0;
				}
			}
			else {
				if (e != this && e->solid && e->bounds().intersects(bounds()) && e->position.y + e->size.y > position.y + size.y) {
					position.y -= jump_velocity;
					jumping = false;
					jump_velocity = 0;
				}
			}
		}
	}

	// HAZARDS
	for (auto e : getCurrentChunk()->entities) {
		// Spike
		if (e->type == "spike" && e->bounds().intersects(bounds())) {
			alive = false;
			break;
		}
		// Walker (sides)
		else if (e->type == "walker" && e->bounds().intersects(bounds())) {
			if (position.y > e->position.y && position.y < e->position.y + e->size.y) {
				alive = false;
			}
			break;
		}
	}
}

void Player::render(sf::RenderWindow &window) {
	//	sf::RectangleShape shape;
	//	shape.setPosition(position);
	//	shape.setSize(size);
	//	window.draw(shape);
	sprite.setPosition(position);
	window.draw(sprite);
}

void Player::input() {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		if (!jumping && can_jump && !tapped_jump) {
			tapped_jump = true;
			jump();
		}
	}
	// Cut jump short at a certain range
	else if (!bounced_on_enemy_head && jumping && jump_velocity <= jump_velocity_min && jump_velocity >= jump_velocity_min_end) {
		jumping = false;
	}

	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		tapped_jump = false;
	}

	// LEFT
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		last_side = -1;
		// Accelerate
		//		if (jumping) {
		//			if (x_velocity > -x_velocity_max) {
		//				x_velocity -= x_acceleration_air;				
		//			}
		//		} else {
		if (x_velocity > -x_velocity_max) {
			x_velocity -= x_acceleration;
		}
		//		}

		// Move then collision check
		position.x += x_velocity;
		if (touchingPlatforms()) position.x -= x_velocity;

		// Face sprite left
		if (up_side) {
			sprite.setOrigin({ sprite.getLocalBounds().width, 0 });
			sprite.setScale({ -1, 1 });
		}
		else {
			sprite.setOrigin({ sprite.getLocalBounds().width, sprite.getLocalBounds().height });
			sprite.setScale({ -1, -1 });
		}
	}
	// RIGHT
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		last_side = 1;
		// Accelerate
		//		if (jumping) {
		//			if (x_velocity < x_velocity_max) {
		//				x_velocity += x_acceleration_air;				
		//			}
		//		} else {
		if (x_velocity < x_velocity_max) {
			x_velocity += x_acceleration;
		}
		//		}

		// Move then collision check
		position.x += x_velocity;
		if (touchingPlatforms()) position.x -= x_velocity;

		// Face sprite right
		if (up_side) {
			sprite.setOrigin({ 0, 0 });
			sprite.setScale(1, 1);
		}
		else {
			sprite.setOrigin({ 0, sprite.getLocalBounds().height });
			sprite.setScale(1, -1);
		}
	}
	else x_velocity = 0;

	// flip or enter door
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		if (!tapped_key) {
			tapped_key = true;

			// Enter door
			for (auto e : getCurrentChunk()->entities) {
				if (e->type == "door" && e->bounds().intersects(bounds())) {
					Door *door = dynamic_cast<Door*>(e);
					game->loadLevel(door->target_level);
					game->restartLevel();
					break;
				}
			}

			flip(!up_side);

			// Flip the sprite
			if (up_side) {
				sprite.setScale(sprite.getScale().x, 1);
				sprite.setOrigin({ sprite.getOrigin().x, 0 });
			}
			else {
				sprite.setScale(sprite.getScale().x, -1);
				sprite.setOrigin({ sprite.getOrigin().x, sprite.getLocalBounds().height });
			}
		}
	}
	else {
		tapped_key = false;
	}

	// Z key
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
		if (!tapped_z) {
			tapped_z = true;

			// Mattock
			if (current_item != NULL && current_item->type == "picaxe") {
				for (auto e : getCurrentChunk()->entities) {
					if (e->type == "sand_block" && e->getTilePos().y == this->getTilePos().y && e->getTilePos().x == this->getTilePos().x + last_side) {
						e->alive = false;
						break;
					}
				}
			}
		}
	}
	else tapped_z = false;
}

bool Player::touchingPlatforms() {
	for (auto e : getCurrentChunk()->entities) {
		if (e != this && e->solid && !e->hostile && e->bounds().intersects(bounds())) {
			return true;
		}
		else if (e->hostile && e->bounds().intersects(bounds())) {
			alive = false;
			break;
		}
	}
	return false;
}

Chunk *Player::getCurrentChunk() {
	for (auto c : game->chunks) {
		if (c->bounds().intersects(this->bounds())) return c;
	}
	return NULL;
}
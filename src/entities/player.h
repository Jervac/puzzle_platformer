#ifndef PLAYER_H
#define PLAYER_H

#include "entity.h"

struct Chunk;

struct Player : public Entity {

    Entity *current_item = NULL;
	int last_side = 1; // Last x direction. Default is 1 (right)
    bool tapped_key = false;
    bool tapped_jump = false;
	bool tapped_z = false;

	Player(Game *ngame);

    void update();
    void render(sf::RenderWindow &window);
    void input();
    
    bool touchingPlatforms();

	Chunk *getCurrentChunk();
};
#endif

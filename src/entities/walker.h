#ifndef WALKER_H
#define WALKER_H

#include "entity.h"

struct Walker : public Entity {
    Walker(Game *ngame, sf::Vector2f nposition, int nleft_right); // 0 = left | 1 = right

    void update();
    void render(sf::RenderWindow &window);
    
    // True when it's about to walk off the ground
    bool reachedEdge();
};
#endif

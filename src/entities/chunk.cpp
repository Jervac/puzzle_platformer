#include "chunk.h"
#include "shooter.h"
#include "bullet.h"
#include "../game.h"
#include "player.h"
#include "../utils.h"

Chunk::Chunk(Game *_game, sf::Vector2f _position, sf::Vector2f _size) : Entity(_game, _position, _size) {
	type = "chunk";
	solid = false;
}

void Chunk::update() {
/*
	if (!game->paused_key) {
		for (auto e : entities) {
			if (e->type == "player") {
				Player *player = dynamic_cast<Player*>(e);
				player->input();
				break;
			}
		}
	}
	*/
	for (auto e : entities) e->update();
}

void Chunk::render(sf::RenderWindow &window) {
	println(entities.size());
	for (auto e : entities) {
		e->render(window);

		/*
		// Render bullets from shooter
		if (e->type == "shooter") {
			Shooter *shooter = dynamic_cast<Shooter*>(e);

			// TODO: Delete the bullets if I don't already somewhere!!!
			for (auto b : shooter->bullets) {
				if (b->alive && b->bounds().intersects(bounds())) {
					b->render(window);
				}
			}
		}
		*/

	}

	if (game->debug_mode) {
		sf::RectangleShape shape;
		shape.setPosition(position);
		shape.setSize(size);
		shape.setFillColor(sf::Color::Transparent);
		shape.setOutlineThickness(2);
		shape.setOutlineColor(sf::Color::Yellow);
		window.draw(shape);
	}
}
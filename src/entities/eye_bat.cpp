#include "eye_bat.h"
#include "math.h"
#include "../game.h"
#include "../utils.h"

EyeBat::EyeBat(Game *ngame, sf::Vector2f nposition, sf::Vector2f nend_position)
: Entity(ngame, nposition) {
	type = "eye_bat";
	hostile = true;
	position = nposition;
	start_position = nposition;
	end_position = nend_position;
    velocity = 8;

	if (end_position.x == NULL || end_position.y == NULL) {
        auto_move = true;
	}
}

void EyeBat::update() {

    if (auto_move) {

        // NOTE: reached_end is being used as a check for if
        // the bat hit a wall while moving right
        if (!reached_end) position.x += velocity;
        else position.x -= velocity;

        for (auto e : game->entities) {
            if (e != this && e->bounds().intersects(this->bounds())) {
                if (reached_end) position.x += velocity;
                else if (!reached_end) position.x -= velocity;
                reached_end = !reached_end;
                break;
            }
        }

    } // Move along vector towards target position
    else {
        float dx, dy;

        if (!reached_end) {
            dx = end_position.x - position.x;
            dy = end_position.y - position.y;
        } else {
            dx = start_position.x - position.x;
            dy = start_position.y - position.y;
        }

        float len = sqrt((dx * dx) + (dy * dy));
        if (len != 0) {
            dx /= len;
            dy /= len;
        }
        position.x += dx * velocity;
        position.y += dy * velocity;

        // Collision check is in center of target tile
        Rect target_bounds(0, 0, 1, 1);
        if (!reached_end) {
            target_bounds = Rect(end_position.x + (TILE_SIZE / 2), end_position.y + (TILE_SIZE / 2), 1, 1);
        } else {
            target_bounds = Rect(start_position.x + (TILE_SIZE / 2), start_position.y + (TILE_SIZE / 2), 1, 1);
        }

        // Go other way if reached target
        if (bounds().intersects(target_bounds)) reached_end = !reached_end;
    }
}

void EyeBat::render(sf::RenderWindow &window) {
	sf::RectangleShape shape;
	shape.setPosition(position);
	shape.setSize(size);
	shape.setFillColor(sf::Color::Green);
	window.draw(shape);
}

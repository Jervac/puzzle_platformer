 #include "switch.h"
#include "../game.h"
#include "../utils.h"
#include "toggle_platform.h"
 
 Switch::Switch(Game *ngame, sf::Vector2f nposition, std::string nname_tag, bool non)
     : Entity(ngame, nposition) {
     type = "switch";
     name_tag = nname_tag;
     on = non;
     solid = false;
 }
 
 void Switch::update() {
     
     // Player toggles switch (assumes 
     bool player_collision = false;
     for (auto e : game->entities) {
         if (e->type == "player" && e->bounds().intersects(bounds())) {
             player_collision = true;
             if (!touching_player) {
                 touching_player = true;
                 toggleState();
             }
             break;
         }
     }
     
     if (!player_collision) {
         touching_player = false;
     }
 }
 
 void Switch::render(sf::RenderWindow &window) {
     sf::RectangleShape shape;
     if (on) shape.setFillColor(sf::Color::Blue);
     else shape.setFillColor(sf::Color::Red);
     shape.setPosition(position);
     shape.setSize(size);
     window.draw(shape);
 }
 
 void Switch::toggleState() {
     on = !on;
     
     // Change state of toggle platforms with the same name_tag
     for (auto e : game->entities) {
         if (e->type == "toggle_platform") {
             TogglePlatform *t_platform = dynamic_cast<TogglePlatform*>(e);
             if (t_platform->name_tag == name_tag) {
                 t_platform->on = on;
             }
         }
     }
}
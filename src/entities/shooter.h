#ifndef SHOOTER_H
#define SHOOTER_H

#include "entity.h"
#include "../Chronometer.h"

struct Game;
struct Bullet;

struct Shooter : public Entity {
    // TODO: clear out bullets
    ~Shooter();
    Shooter(Game *ngame, sf::Vector2f nposition, bool nleft_right, bool nup_side, float nshot_delay);
    
    std::vector<Bullet*> bullets;
    sftools::Chronometer shot_timer;
    float shot_delay = 1;
    
    void update();
    void render(sf::RenderWindow &window);
    void shoot();
};
#endif

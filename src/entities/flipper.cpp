#include "flipper.h"

Flipper::Flipper(Game *ngame, sf::Vector2f nposition) 
: Entity(ngame, nposition) {
    type = "flipper";
    solid = true;
}

void Flipper::update() {}

void Flipper::render(sf::RenderWindow &window) {
    sf::RectangleShape shape;
    shape.setPosition(position);
    shape.setSize(size);
    shape.setFillColor(sf::Color::White);
    window.draw(shape);
}

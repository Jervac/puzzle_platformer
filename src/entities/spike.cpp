#include "spike.h"

Spike::Spike(Game *ngame, sf::Vector2f nposition)
	: Entity(ngame, nposition) {
	type = "spike";
	solid = false;
}

void Spike::update() {}

void Spike::render(sf::RenderWindow &window) {
	sf::RectangleShape shape;
	shape.setPosition(position);
	shape.setSize(size);
	shape.setFillColor(sf::Color::Red);
	window.draw(shape);
}

#include "mattock.h"
#include "../game.h"
#include "player.h"
#include "../utils.h"

Mattock::Mattock(Game *_game, sf::Vector2f _position) : Entity(_game, _position) {
    type = "picaxe";
    hostile = false;
    solid = false;
	texture.loadFromFile("data/items/picaxe.png");
	sprite.setTexture(texture);
}

void Mattock::update() {
    if (!collected) {
        for (auto e : game->entities) {
            if (e->type == "player" && e->bounds().intersects(this->bounds())) {
                Player *player = dynamic_cast<Player*>(e);

                player->current_item = this;
                collected = true;
                println("Picked up a Pickaxe!");
				size.x = TILE_SIZE/4;
				size.y = TILE_SIZE/4;
				break;
            }
        }
    }
}

void Mattock::render(sf::RenderWindow &window) {
    if (!collected) {
		sprite.setPosition(position);
		sprite.setScale(size.x / sprite.getLocalBounds().width, size.y / sprite.getLocalBounds().height);
	} // If player is holding it
	else {
		sprite.setPosition(sf::Vector2f(sf::Vector2f(game->player->position.x + game->player->size.x - (size.x/2), game->player->position.y + game->player->size.y/2)));
		sprite.setScale((size.x)/sprite.getLocalBounds().width, (size.y)/sprite.getLocalBounds().height);
	}

	window.draw(sprite);
}
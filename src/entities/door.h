 #ifndef DOOR_H
#define DOOR_H
 
#include "entity.h"
 
 struct Game;
 
 struct Door : public Entity {
     std::string target_level = "overworld_1.map";
     bool on_touch = true; //Switch level on touch?

     Door(Game *ngame, sf::Vector2f nposition, std::string ntarget_level, bool _on_touch);

     void update();
     void render(sf::RenderWindow &window);
 };
 #endif
#include "spawn.h"

Spawn::Spawn(Game *ngame, sf::Vector2f nposition)
	: Entity(ngame, nposition) {
	type = "spawn";
	solid = false;
}

void Spawn::update() {}

void Spawn::render(sf::RenderWindow &window) {
	sf::RectangleShape shape;
	shape.setPosition(position);
	shape.setSize(size);
	shape.setFillColor(sf::Color(0, 0, 0, 0));
	shape.setOutlineThickness(2);
	shape.setOutlineColor(sf::Color::Yellow);
	window.draw(shape);
}

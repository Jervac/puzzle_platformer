#include "sand_block.h"

SandBlock::SandBlock(Game *_game, sf::Vector2f _position) : Entity(_game, _position) {
	type = "sand_block";
	solid = true;
	texture.loadFromFile("data/sand_block.png");
	sprite.setTexture(texture);
	sprite.setScale(size.x / sprite.getLocalBounds().width, size.y / sprite.getLocalBounds().height);
}

void SandBlock::update() {
	updateGravity();
}

void SandBlock::render(sf::RenderWindow &window) {
	sprite.setPosition(position);
	window.draw(sprite);
}
#ifndef BULLET_H
#define BULLET_H

#include "entity.h"

struct Game;

struct Bullet : public Entity {
	Bullet(Game *ngame, sf::Vector2f nposition, bool nleft_right);
	
	void update();
	void render(sf::RenderWindow &window);
};
#endif

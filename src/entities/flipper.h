#ifndef FLIPPER_H
#define FLIPPER_H

#include "entity.h"

struct Game;

struct Flipper : public Entity {
	Flipper(Game *ngame, sf::Vector2f nposition);
	void update();
	void render(sf::RenderWindow &window);
};
#endif

 #ifndef TOGGLE_PLATFORM_H
#define TOGGLE_PLATFORM_H
 
#include "platform.h"
 
 struct Game;
 
 struct TogglePlatform : Platform {
     TogglePlatform(Game *ngame, sf::Vector2f nposition, std::string nname_tag, bool non);
     
     std::string name_tag = "default_name";
     bool on = false;
     
     void update();
     void render(sf::RenderWindow &window);
 };
 #endif
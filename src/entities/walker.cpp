#include "walker.h"
#include "../game.h"
#include "../utils.h"
#include "../globals.h"
#include "math.h"

Walker::Walker(Game *ngame, sf::Vector2f nposition, int nleft_right)
: Entity(ngame, nposition) {
    type = "walker";
    hostile = true;
	size.x = TILE_SIZE;
	size.y = TILE_SIZE;
    velocity = 2.5f;
    
    texture.loadFromFile("data/walker.png");
    sprite.setTexture(texture);
    sprite.setPosition(position);
    sprite.setScale(size.x / sprite.getLocalBounds().width, size.y / sprite.getLocalBounds().height);
}

void Walker::update() {
		// Ignore gravity since this entity is meant to not fall of edges anyways
		//updateGravity();

		// Movement in incriments

		// RIGHT
		if (left_right) {
			for (int i = 0; i < velocity; i++) {
				position.x++;

				// Turn around when at edge
				if (reachedEdge()) {
					position.x -= velocity;
					left_right = !left_right;
				}
				// X axis collision response
				else {
					for (auto e : game->entities) {
						if (e != this && e->bounds().intersects(bounds())) {
							if (e->type == "player") {
								e->alive = false;
							}
							position.x -= velocity;
							left_right = !left_right;
							break;
						}
					}
				}
			}
		}
		// LEFT
		else {
			for (int i = 0; i < velocity; i++) {
				position.x--;

				// Turn around when at edge
				if (reachedEdge()) {
					position.x += velocity;
					left_right = !left_right;
				}
				// X axis collision response
				else {
					for (auto e : game->entities) {
						if (e != this && e->bounds().intersects(bounds())) {
							//position.x = e->position.x + e->size.x;
							if (e->type == "player") {
								e->alive = false;
							}
							position.x += velocity;
							left_right = !left_right;
							break;
						}
					}
				}
			}
		}

		standing_on_flipper = false;
		Rect under_bounds(0, 0, 0, 0);
		if (up_side) under_bounds = Rect(position.x, position.y + size.y, TILE_SIZE, TILE_SIZE);
		else under_bounds = Rect(position.x, position.y - TILE_SIZE, TILE_SIZE, TILE_SIZE);

		for (auto e : game->entities) {
			// Standing on flipper
			if (e->type == "flipper" && under_bounds.intersects(e->bounds())) {
				standing_on_flipper = true;
			}
		}
	}

void Walker::render(sf::RenderWindow &window) {
    /*
    sf::RectangleShape shape;
    shape.setPosition(position);
    shape.setSize(size);
    shape.setFillColor(sf::Color::Green);
    window.draw(shape);
    */
    sprite.setPosition(position);
    window.draw(sprite);
}

bool Walker::reachedEdge() {
	Rect rect(getTilePos().x*TILE_SIZE, getTilePos().y*TILE_SIZE, TILE_SIZE, TILE_SIZE);

	// Right check
    if (left_right) {
        if (up_side) rect = Rect(position.x + size.x, position.y + size.y, TILE_SIZE, TILE_SIZE);
        else rect = Rect(position.x + size.x, (position.y - TILE_SIZE), TILE_SIZE, TILE_SIZE);
        
        for (auto e : game->entities) {
            if (e != this && e->solid && e->bounds().intersects(rect)) {
                return false;
            }
        }
    } 
    // Left check
    else {
        if (up_side) rect = Rect(position.x - size.x, position.y + size.y, size.x, size.y);
        else rect = Rect(position.x - TILE_SIZE, (position.y - TILE_SIZE), TILE_SIZE, TILE_SIZE);
        
        for (auto e : game->entities) {
            if (e != this && e->solid && e->bounds().intersects(rect)) {
                return false;
            }
        }
    }
    return true;
}

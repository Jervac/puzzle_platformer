#ifndef CHUNK_H
#define CHUNK_H

#include "entity.h"

struct Game;
struct Entity;

struct Chunk : public Entity {
	std::vector<Entity*> entities;

	Chunk(Game *_game, sf::Vector2f _position, sf::Vector2f _size);

	void update();
	void render(sf::RenderWindow &window);
};
#endif
#ifndef EYE_BAT_H
#define EYE_BAT_H

#include "entity.h"

struct Game;

struct EyeBat : public Entity {
	EyeBat(Game *ngame, sf::Vector2f nposition, sf::Vector2f nend_position);
	sf::Vector2f start_position;
	sf::Vector2f end_position = sf::Vector2f(NULL, NULL);
	bool reached_end = false; // Toggles wether moving to start or end position
    bool auto_move = false;
	void update();
	void render(sf::RenderWindow &window);
};
#endif

#ifndef PLATFORM_H
#define PLATFORM_H

#include "entity.h"

struct Platform : Entity {
	Platform(Game *ngame, sf::Vector2f nposition);
	void update();
	void render(sf::RenderWindow &window);
};
#endif

 #ifndef SWITCH_H
#define SWITCH_H
 
#include "entity.h"
 
 struct Game;
 
 struct Switch : public Entity {
     Switch(Game *ngame, sf::Vector2f nposition, std::string nname_tag, bool non);
     
     // Tag of switch to find matching platforms of same tag
     std::string name_tag;
     bool on = false;
     bool touching_player = false;
     
     void update();
     void render(sf::RenderWindow &window);
     // Turn switch on / off
     void toggleState();
 };
 #endif
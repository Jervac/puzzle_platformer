 #include "door.h"
#include "../game.h"
#include "../utils.h"

 Door::Door(Game *ngame, sf::Vector2f nposition, std::string ntarget_level, bool _on_touch)
     : Entity(ngame, nposition) {
     type = "door";
     target_level = ntarget_level;
     size.x = TILE_SIZE;
     size.y = TILE_SIZE;
     solid = false;
     on_touch = _on_touch;
 }
 
 void Door::update() {

     // Switch level if player touches door and door is on_touch
     if (on_touch) {
         for (auto c : game->entities) {
             if (c->type == "player" && this->bounds().intersects(c->bounds())) {
                 println("Switching to next level automatically but not really since this is commented code in door.cpp");
//                 game->loadLevel("1.map");
//                 game->restartLevel();
                 break;
             }
         }
     }
 }
 
 void Door::render(sf::RenderWindow &window) {
     sf::RectangleShape shape;
     shape.setPosition(position);
     shape.setSize(size);
     shape.setFillColor(sf::Color::Green);
     if (on_touch) shape.setFillColor(sf::Color(20, 100, 20, 255));
     window.draw(shape);
 }
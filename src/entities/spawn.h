#ifndef SPAWN_H
#define SPAWN_H

#include "entity.h"

struct Spawn : public Entity {
	Spawn(Game *ngame, sf::Vector2f nposition);
	void update();
	void render(sf::RenderWindow &window);
};
#endif

#include "shooter.h"
#include "bullet.h"
#include "../utils.h"

Shooter::Shooter(Game *ngame, sf::Vector2f nposition, bool nleft_right, bool nup_side, float nshot_delay)
: Entity(ngame, nposition) {
    type = "shooter";
    left_right = nleft_right;
    up_side = nup_side;
    shot_delay = nshot_delay;
    
    shot_timer.reset(true);
}

Shooter::~Shooter() {
    bullets.clear();
}

void Shooter::update() {
    for (auto b : bullets) {
        if (b->alive) b->update();
    }
    
    // If time to shoot again
    // Objects deleted after update so 
    if (shot_timer.getElapsedTime().asSeconds() >= shot_delay) {
        shoot();
        
        // Clear dead bullets
        // Deleting an element alone invalidates the vector so more
        // work is needed here
        
        for(size_t i = 0; i < bullets.size();) { // no i++ here
            if(!bullets[i]->alive) {
                delete bullets[i];
                bullets.erase(bullets.begin() + i);
            }
            else {
                i++;
            }
        }
    }
    
}

void Shooter::render(sf::RenderWindow &window) {
    sf::RectangleShape shape;
    shape.setPosition(position);
    shape.setSize(size);
    shape.setFillColor(sf::Color::Green);
    
    // Barrel
    sf::RectangleShape barrel;
    barrel.setSize(sf::Vector2f(10, 5));
    if (left_right) barrel.setPosition(sf::Vector2f(position.x + size.x, position.y));
    else barrel.setPosition(sf::Vector2f(position.x - barrel.getSize().x, position.y));
    barrel.setFillColor(sf::Color::Red);
    window.draw(shape);
    window.draw(barrel);
}

void Shooter::shoot() {
    shot_timer.reset(true);
    float spawn_x, spawn_y;
    
    if (up_side) spawn_y = position.y;
    else spawn_y = position.y + size.y;
    
    if (left_right) spawn_x = position.x + size.x;
    else spawn_x = position.x - size.x;
    
    bullets.push_back(new Bullet(game, sf::Vector2f(spawn_x, spawn_y), left_right));
    println(left_right);
}
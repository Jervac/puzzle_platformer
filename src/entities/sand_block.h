#ifndef SAND_BLOCK_H
#define SAND_BLOCK_H

#include <SFML/Graphics.hpp>
#include "entity.h"

struct SandBlock : public Entity {

	SandBlock(Game *_game, sf::Vector2f _position);
	void update();
	void render(sf::RenderWindow &window);
};
#endif
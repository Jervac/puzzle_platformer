#ifndef PICAXE_H
#define PICAXE_H

#include "entity.h"

struct Mattock : public Entity {
	bool collected = false;
    Mattock(Game *_game, sf::Vector2f _position);

    void update();
    void render(sf::RenderWindow &window);
};
#endif
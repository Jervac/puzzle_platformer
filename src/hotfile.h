#ifndef HOTFILE_H
#define HOTFILE_H

#include <iostream>
#include <fstream>
#include <vector>
#include <fstream>
#include <algorithm>

// NOTE: item is really a variable in file

// ***************************************************************
// TODO: Change occurances of item to variables for less ambiguity
// ***************************************************************

struct Hotfile {
	Hotfile(std::string nfile);

	bool debug = false; // prints helpful things when in debug
	std::string file;
	std::vector<std::string> file_items; // count of variables / lines in file
	std::string delimiter = " ";
	int file_item_count = 0;    // needed because file_items.size() doesn't return correct value

	void hot_load();

	void remove_substrings(std::string &s, std::string &p);

	// item name, index string to add
	void add_index(std::string nitem, std::string index);

	// Clear an item's data and add in the new indexes
	void reset_item(std::string nitem, std::string indexes);

	// returns string param at given index + 1 of an item name.
	// +1 so index 0 isn't the item name
	std::string item_at_index(std::string item, int index);
	
	void set_file(std::string nfile);
};
#endif

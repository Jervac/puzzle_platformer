#ifndef UTILS_H
#define UTILS_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>

////////////////////////////////////////
// Utilities to simplify code verbosity
////////////////////////////////////////

template <typename T>
inline void println(T const& t) {
	std::cout << t << std::endl;
};

template<typename T>
inline void debug_println(T const &t) {
	std::cout << "[debug] " << t << std::endl;
};

// Removes T from vec if vec contains T

template<typename T>
inline void remove_from_vector(T const &t, std::vector<T> &vec) {
	for (int i = 0; i < vec.size(); i++) {
		if (vec[i] == t) {
			vec.erase(vec.begin() + i);
			break;
		}
	}
}

// TODO: Make my own collection type for handling these things
// TODO: What does inline void mean?

template<typename T>
inline bool vector_contains(T const &t, std::vector<T> &vec) {
	for (auto v : vec) {
		if (v == t) {
			return true;
		}
	}
	return false;
}

int rand(int min, int max);


int rand(int min, int max);

struct Rect {
	Rect(float nx, float ny, float nwidth, float nheight);
	float x = 0, y = 0;
	float width = 0, height = 0;
	bool intersects(Rect b);
	sf::Vector2f getPosition();
	void setPosition(sf::Vector2f nposition);
	sf::Vector2f getSize();
	void setSize(sf::Vector2f nsize);
};
#endif
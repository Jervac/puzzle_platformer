#include "hotfile.h"
#include "utils.h"
#include <sstream>

Hotfile::Hotfile(std::string nfile) {
	set_file(nfile);
}

void Hotfile::hot_load() {
	std::ifstream f(file);
	
	if (!f) {
		std::cout << "couldn't load " << file << std::endl;
	} else {
		std::string line;
		file_item_count = 0;
		file_items.clear();

		while (std::getline(f, line)) {
			file_item_count++;
			file_items.push_back(line);
		}

		// prints out file contents
		if (debug) {
			std::cout << "[debug]: hotloaded [" << file << "] : " << std::endl;
			for (int i = 0; i < file_item_count; ++i) {
				std::cout << "[debug]: " << file_items[i] << std::endl;
			}
			std::cout << std::endl;
		}
	}
	f.close();
}

// TODO: simplify this. The while loop likely isn't needed
// and only contributes to less understandable code
std::string Hotfile::item_at_index(std::string item, int index) {

	// while loop modifies index, but it's needed later
	// so i use this copy later
	int copy_of_index = index;

	// for each line in the file
	for (int i = 0; i < file_item_count; ++i) {
		std::string current_line = file_items[i];
		std::string token;  // the value at "index"
		bool found_current_line = false;
		
		while ((index = current_line.find(delimiter)) != std::string::npos) {
			token = current_line.substr(0, index);

			// if target is found, split it
			if (token == item) {
				found_current_line = true;
				
				// remove the name of item
				current_line.erase(0, index + delimiter.length());

				std::istringstream ss(current_line);
				std::string token_2;
				int current_line_index = 0;

				// return the string being searched for
				while(std::getline(ss, token_2, ' ')) {
					if(current_line_index == copy_of_index) {
						return token_2;
					}
					current_line_index++;
				}
			}
			
			// removes chars from current_line until it is the index item we want
			current_line.erase(0, index + delimiter.length());  
		}
	}

	if (debug) {
		std::cout << "Failed to hotload [" << file << "]. Couldn't find [" << item << "]." << std::endl;
	}
	return NULL;
}

void Hotfile::remove_substrings(std::string &s, std::string &p) {
	std::string::size_type n = p.length();
	for (std::string::size_type i = s.find(p);
		 i != std::string::npos;
		 i = s.find(p))
		s.erase(i, n);
}

void Hotfile::add_index(std::string item, std::string index) {
	file_items.push_back(index);
	std::string new_file;

	// Put index in item in to new_file
	for (int i = 0; i < file_item_count; i++) {
		std::string line = file_items[i];
		if (line.find(item) != std::string::npos) {
			line += " " + index + " ";
			file_items[i] = line;
			new_file += line + "\n";
		} else
			new_file += line + "\n";
	}

	// Put saved contents into the file
	try {
		// Clear current file and write new_file to it
		std::ofstream ofs;
		ofs.open(file, std::ofstream::out | std::ofstream::trunc);
		ofs << new_file;
		ofs.close();
	} catch (...) {}
}

void Hotfile::reset_item(std::string item, std::string indexes) {
	std::string new_file;

	// Put current data in file if it's not target item
	for (int i = 0; i < file_item_count; i++) {
		std::string line = file_items[i];
		// Line is target, replace it with item +
		// indexes for the data
		if (line.find(item) != std::string::npos) {
			line = item + indexes;
			file_items[i] = line;
			new_file += line + "\n";
		} else
			new_file += line + "\n";
	}

	// Put contents into the file
	try {
		std::ofstream ofs;
		ofs.open(file, std::ofstream::out | std::ofstream::trunc);

		ofs << new_file;
		ofs.close();
	} catch (...) {}
}

void Hotfile::set_file(std::string nfile) {
	file = nfile;
}

#ifndef GLOBALS_H
#define GLOBALS_H

#include <vector>
#include <string>

extern const int WINDOW_WIDTH;
extern const int WINDOW_HEIGHT;
extern const std::string WINDOW_TITLE;
extern const int TILE_SIZE;
extern const float FLIPPER_HEIGHT;
#endif

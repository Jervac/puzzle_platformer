#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include <vector>

struct Platform;
struct Player;
struct Entity;
struct Hotfile;
struct Rect;
struct SuperFile;
struct Chunk;

struct Game {
    sf::RenderWindow window;
    sf::View camera;
    std::vector<Entity*> entities; // Stores entities until they're sorted into chunks.
	std::vector<Chunk*> chunks;
    SuperFile *current_level;
    Rect *editor_rect = NULL; // TODO: Remove this if not needed
	Rect *chunk_rect = NULL;
	// TODO: CHANGE THIS NAME TO selected_entity !!!!!
    Entity *last_selected_entity = NULL; // Last created or clicked entity (for now is last created)
	Player *player = NULL;
    
    bool paused = false;
    bool paused_key = true;
    bool restart_key = false;
    bool window_focused = true;
    bool tapped_key = false;
    bool tapped_left_mouse = false;
    int editor_mode = 0;
    bool debug_mode  = false;
    bool door_console = true; // TODO: RENAME THIS TO JUST BE CONSOLE
    bool limited_camera_x = false;
    bool limited_camera_y = false;
    
    int previous_FPS = 0;
    int FPS = 0;
    int editor_id = 0;
    
    float camera_velocity = 14;
    float camera_x_max = 0;
    float camera_x_min = 0;
    float camera_y_max = 0;
    float camera_y_min = 0;
    
    char door_console_buffer[255];
    std::string door_console_text = "";
    
    void run();
    void init();
    void update();
    void render();
    void input();
    void restartLevel();
    void loadLevel(std::string file_name);
    void saveLevel();
};
#endif

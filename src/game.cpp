#include "game.h"
#include <iostream>
#include <sstream>
#include <math.h>
#include "utils.h"
#include "globals.h"
#include "hotfile.h"
#include "super_file.h"
#include "entities/player.h"
#include "entities/platform.h"
#include "entities/flipper.h"
#include "entities/spawn.h"
#include "entities/spike.h"
#include "entities/walker.h"
#include "entities/door.h"
#include "entities/switch.h"
#include "entities/toggle_platform.h"
#include "entities/eye_bat.h"
#include "entities/shooter.h"
#include "entities/bullet.h"
#include "entities/mattock.h"
#include "entities/sand_block.h"
#include "entities/chunk.h"
#include <imgui.h>
#include <imgui-SFML.h>

//////////////////////////////////////////////////
// TODO: put IDs of different entities in globals
//////////////////////////////////////////////////
void Game::init() {
	window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), WINDOW_TITLE, sf::Style::Default);
	sf::Vector2i window_pos(sf::VideoMode::getDesktopMode().width / 2 - (window.getSize().x / 2), sf::VideoMode::getDesktopMode().height / 2 - (window.getSize().y / 2));

	window.setPosition(window_pos);
	// Without some limit CPU usage goes up and and uses high FPS
	window.setFramerateLimit(60);
	window.setVerticalSyncEnabled(true);

	camera.setSize(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT));
	camera.setCenter(sf::Vector2f(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2));
	camera.zoom(1.5f);

	ImGui::SFML::Init(window);

	loadLevel("data/overworld_0.map");
}

void Game::update() {
	if (!paused) {

		// Delete dead entities
		// NOTE: Restarts if player dies
		std::vector<Entity*>::iterator i = entities.begin();
		bool playerDead = false;
		while (i != entities.end()) {
			bool isAlive = (*i)->alive;
			if (!isAlive) {
				if ((*i)->type == "player") playerDead = true;
				delete (*i);
				i = entities.erase(i);
				if (playerDead) {
					restartLevel();
					break;
				}
			}
			else ++i;
		}
		

		// Update entities
		for (auto c : chunks) {
			c->update();
		}
	}
}

void Game::render() {
	window.clear(sf::Color(20, 20, 20, 255));
	window.setView(camera);

	// Camera follow player
		for (auto e : entities) {
			if (e->type == "player") {
				const float lerp = .1f; // Jittery when this number is odd
				sf::Vector2f previous_position = camera.getCenter();

				// Limited X
				if (limited_camera_x) {
					camera.setCenter(sf::Vector2f(camera.getCenter().x + (e->position.x - camera.getCenter().x) * lerp, camera.getCenter().y));

					// Return to previous position if out of bounds
					if (camera.getCenter().x > camera_x_max) {
						camera.setCenter(sf::Vector2f(previous_position.x, camera.getCenter().y));
					}
					else if (camera.getCenter().x < camera_x_min) {
						camera.setCenter(sf::Vector2f(previous_position.x, camera.getCenter().y));
					}
				}

				// Limited Y
				if (limited_camera_y) {
					camera.setCenter(sf::Vector2f(camera.getCenter().x, camera.getCenter().y + (e->position.y - camera.getCenter().y) * lerp));

					// Return to previous position if out of bounds
					if (camera.getCenter().y > camera_y_max) {
						camera.setCenter(sf::Vector2f(camera.getCenter().x, previous_position.y));
					}
					else if (camera.getCenter().y < camera_y_min) {
						camera.setCenter(sf::Vector2f(camera.getCenter().x, previous_position.y));
					}
				}

				// No limits
				if (!limited_camera_x && !limited_camera_y) {
					camera.setCenter(sf::Vector2f(camera.getCenter().x + (e->position.x - camera.getCenter().x) * lerp, camera.getCenter().y + (e->position.y - camera.getCenter().y) * lerp));
				}
				break;
			}
		}

	Rect view_bounds(0, 0, 0, 0);
	view_bounds.setPosition(sf::Vector2f(camera.getCenter().x - (camera.getSize().x / 2),
		camera.getCenter().y - (camera.getSize().y / 2)));
	view_bounds.setSize(camera.getSize());

	// Render entities on screen
	
	for (auto e : entities) {
		if (e->bounds().intersects(view_bounds)) {
			if (e->type == "player") e->render(window);
		}

		// Render bullets from shooter
		if (e->type == "shooter") {
			Shooter *shooter = dynamic_cast<Shooter*>(e);

			for (auto b : shooter->bullets) {
				if (b->alive && b->bounds().intersects(view_bounds)) {
					b->render(window);
				}
			}
		}
	}
	

	for (auto c : chunks) c->render(window);

	if (chunk_rect != NULL) {
		sf::RectangleShape shape;
		shape.setSize(chunk_rect->getSize());
		shape.setPosition(chunk_rect->getPosition());
		shape.setFillColor(sf::Color::Transparent);
		shape.setOutlineThickness(2);
		shape.setOutlineColor(sf::Color::White);
		window.draw(shape);
	}



	// ImGui stuff
	if (debug_mode) {
		sf::Vector2f mouse_pos_world = window.mapPixelToCoords(sf::Mouse::getPosition(window));

		// Debug info
		ImGui::Begin("Debug");
		ImGui::SetWindowPos(ImVec2(10, 10), 1);
		ImGui::SetWindowSize(ImVec2(200, 300), 1);

		ImGui::Text(("FPS: " + std::to_string(previous_FPS)).c_str());
		ImGui::Text(("editor_mode: " + std::to_string(editor_mode)).c_str());
		ImGui::Text(("editor_id: " + std::to_string(editor_id)).c_str());
		ImGui::Text(("camera_x: " + std::to_string(camera.getCenter().x)).c_str());
		ImGui::Text(("camera_y: " + std::to_string(camera.getCenter().y)).c_str());
		ImGui::Text(("camera_x_min: " + std::to_string(camera_x_min)).c_str());
		ImGui::Text(("camera_x_max: " + std::to_string(camera_x_max)).c_str());
		ImGui::Text(("camera_y_min: " + std::to_string(camera_y_min)).c_str());
		ImGui::Text(("camera_y_max: " + std::to_string(camera_y_max)).c_str());
		ImGui::Text(("mouse_x: " + std::to_string(mouse_pos_world.x)).c_str());
		ImGui::Text(("mouse_y: " + std::to_string(mouse_pos_world.y)).c_str());
		ImGui::Text(("mouse_tile_x: " + std::to_string(round(mouse_pos_world.x / TILE_SIZE))).c_str());
		ImGui::Text(("mouse_tile_y: " + std::to_string(round(mouse_pos_world.y / TILE_SIZE))).c_str());

		if (last_selected_entity != NULL) {
			ImGui::Text(("last_selected_entity.x: " + std::to_string(last_selected_entity->position.x)).c_str());
		}
		ImGui::End();

		// Key bindinds
		ImGui::Begin("Key Bindings");
		ImGui::SetWindowPos(ImVec2(1280 - 300 - 10, 10), 1);
		ImGui::SetWindowSize(ImVec2(300, 200), 1);

		ImGui::Text("F1: Toggle debug_mode");
		ImGui::Text("F2: Toggle editor_mode");
		ImGui::Text("F3: Hotload");
		ImGui::Text("F6: Save current level");
		ImGui::Text("");
		ImGui::Text("1: Decrease editor_id");
		ImGui::Text("2: Increase editor_id");
		ImGui::Text("");
		ImGui::Text("Enter: Create object formed with mouse");
		ImGui::End();

		// Check boxes for current entity
		if (last_selected_entity != NULL) {

			ImGui::Begin(last_selected_entity->type.c_str());
			ImGui::SetWindowPos(ImVec2(10, 720 - 400), 1);
			ImGui::SetWindowSize(ImVec2(200, 400), 1);

			// Create array and set the defaults to that of the
			// current entity
			//static
			bool selected_entity_bools[2] = {
				last_selected_entity->left_right,
				last_selected_entity->up_side
			};

			// left_right
			ImGui::Checkbox("##LED1", &selected_entity_bools[0]);
			last_selected_entity->left_right = selected_entity_bools[0];
			ImGui::SameLine();
			ImGui::Text("left_right");

			// up_side
			ImGui::Checkbox("##LED2", &selected_entity_bools[1]);
			last_selected_entity->up_side = selected_entity_bools[1];
			ImGui::SameLine();
			ImGui::Text("up_side");

			ImGui::End();
		}

		// Door console (target_level of selected door)
		if (door_console) {
			ImGui::Begin("Door");
			ImGui::InputText("target_level or type on_touch to toggle that.", door_console_buffer, 255);
			ImGui::End();
		}
	}

}

void Game::input() {
	// Exit window
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::F12)) {
		window.close();
	}

	// Toggle pause
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		if (!paused_key) {
			paused_key = true;
			paused = !paused;
		}
	}
	else paused_key = false;

	// Player input
	if (!paused_key) {
		for (auto e : entities) {
			if (e->type == "player") {
				Player *player = dynamic_cast<Player*>(e);
				player->input();
				break;
			}
		}
	}

	// Increase editor_id
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
		if (!tapped_key) {
			tapped_key = true;
			editor_id++;
		}
	}
	// Decrease editor_id
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
		if (!tapped_key) {
			tapped_key = true;
			editor_id--;
		}
	}
	// toggle editor_mode (1 or 2)
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::RControl) && sf::Keyboard::isKeyPressed(sf::Keyboard::F2)) {
		if (!tapped_key) {
			tapped_key = true;
			if (editor_mode != 2) editor_mode = 2;
			else editor_mode = 1;

			
		}
	}
	// toggle editor_mode (0 or 1)
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::F2)) {
		if (!tapped_key) {
			tapped_key = true;
			if (editor_mode == 2) editor_mode = 0;
			else editor_mode = !editor_mode;

			// Remove editor rect when editor off
			if (!editor_mode) {
				editor_rect = NULL;
			}
		}
	}
	// toggle debug_mode
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::F1)) {
		if (!tapped_key) {
			tapped_key = true;
			debug_mode = !debug_mode;
		}
	}
	// Save current level
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::F6)) {
		if (!tapped_key) {
			tapped_key = true;
			saveLevel();
			println("Saved level!");
		}
	}
	// Restart level
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::R)) {
		if (!debug_mode && !tapped_key) {
			tapped_key = true;
			restartLevel();
		}
	}
	else tapped_key = false;

	// Create / Modify entities
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
		sf::Vector2f mouse_world = window.mapPixelToCoords(sf::Mouse::getPosition(window));
		bool spot_taken = false;

		float tile_x = floor(mouse_world.x / TILE_SIZE);
		float tile_y = floor(mouse_world.y / TILE_SIZE);

		for (auto e : entities) {
			if (mouse_world.x > e->position.x && mouse_world.x < e->position.x + e->size.x &&
				mouse_world.y > e->position.y &&
				mouse_world.y < e->position.y + e->size.y) {
				spot_taken = true;
				last_selected_entity = e;
				break;
			}
		}

		// Create chunk_rect or resize it
		if (chunk_rect == NULL) {
			chunk_rect = new Rect(mouse_world.x, mouse_world.y, 0, 0);
		} else {
			chunk_rect->width  = mouse_world.x - chunk_rect->x;
			chunk_rect->height = mouse_world.y - chunk_rect->y;
		}
		
		// Create a chunk and shift the position in case of negative widths/heights
		if (editor_mode == 2 && chunk_rect != NULL && sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
			Rect target_rect = Rect(chunk_rect->x, chunk_rect->y, abs(chunk_rect->width), abs(chunk_rect->height));
			if (chunk_rect->width < 0)  target_rect.x = chunk_rect->x - abs(chunk_rect->width);
			if (chunk_rect->height < 0) target_rect.y = chunk_rect->y - abs(chunk_rect->height);
			chunks.push_back(new Chunk(this, chunk_rect->getPosition(), chunk_rect->getSize()));
			chunk_rect = NULL;
		}


		// LSHIFT + CLICK Commands that use input box
		if (spot_taken && last_selected_entity != NULL && editor_mode != 2) {
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
				// DOOR target_level
				if (last_selected_entity->type == "door") {
					Door *door = dynamic_cast<Door*>(last_selected_entity);
					if (door_console_buffer == "on_touch") {
						println("On touch switch");
						door->on_touch = !door->on_touch;
					}
					else {
						door->target_level = door_console_buffer;
						std::cout << "new target_level: " << door_console_buffer << std::endl;;
					}
				}
				// SHOOTER shot_delay
				else if (last_selected_entity->type == "shooter") {
					Shooter *shooter = dynamic_cast<Shooter*>(last_selected_entity);
					try {
						shooter->shot_delay = std::atof(door_console_buffer);
						println("Changed shot delay to: " + std::to_string(shooter->shot_delay));
					}
					catch (...) {
						println("Failed to change the shot_delay of this shooter!");
					}
				}
			}
		}

		// LSHIFT + CLICK empty spot to set target for eye_bat
		if (!spot_taken &&
			editor_mode != 2 &&
			last_selected_entity != NULL &&
			last_selected_entity->type == "eye_bat" &&
			sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
			EyeBat *eye_bat = dynamic_cast<EyeBat*>(last_selected_entity);
			eye_bat->end_position = sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE);
			println("Set end_position for an EyeBat!");
		}

		// CREATE
		if (editor_mode && editor_mode != 2 && !spot_taken && !sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
			// Platform
			if (editor_id == 0) {
				entities.push_back(new Platform(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE)));
			}
			// Flipper
			else if (editor_id == 1) {
				entities.push_back(new Flipper(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE)));
			}
			// Spawn
			else if (editor_id == 2) {
				entities.push_back(new Spawn(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE)));
			}
			// Spike
			else if (editor_id == 3) {
				entities.push_back(new Spike(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE)));
			}
			// Walker
			else if (editor_id == 4) {
				Walker *walker = new Walker(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE),
					true);
				walker->position.y = (tile_y * TILE_SIZE) - walker->size.y + TILE_SIZE; // position height properly
				entities.push_back(walker);
			}
			// Door
			else if (editor_id == 5) {
				Door *door = new Door(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE),
					"overworld_0.map", true);
				door->position.y = (tile_y * TILE_SIZE) - door->size.y + TILE_SIZE; // position height properly
				entities.push_back(door);
			}
			// Switch
			else if (editor_id == 6) {
				entities.push_back(new Switch(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE),
					"default_name",
					false));
			}
			// Toggle Platform
			else if (editor_id == 7) {
				entities.push_back(new TogglePlatform(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE),
					"default_name",
					false));
			}
			// EyeBat
			else if (editor_id == 8) {
				entities.push_back(new EyeBat(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE),
					sf::Vector2f(NULL, NULL)));
			}
			// Shooter
			else if (editor_id == 9) {
				entities.push_back(new Shooter(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE),
					true,
					true,
					1.0f));
			}
			// SandBlock
			else if (editor_id == 10) {
				entities.push_back(new SandBlock(this,
					sf::Vector2f(tile_x * TILE_SIZE, tile_y * TILE_SIZE)));
			}
		}
	} // Mouse not being click
	else {
		chunk_rect = NULL;
	}

	// Delete
	if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
		sf::Vector2f mouse_world = window.mapPixelToCoords(sf::Mouse::getPosition(window));
		float tile_x = floor(mouse_world.x / TILE_SIZE);
		float tile_y = floor(mouse_world.y / TILE_SIZE);

		if (entities.size() > 0) {
			for (int i = entities.size() - 1; i >= 0; i--) {
				if (floor(entities.at(i)->position.x / TILE_SIZE) == tile_x &&
					floor(entities.at(i)->position.y / TILE_SIZE) == tile_y) {
					entities.erase(entities.begin() + i);
				}
			}
		}
	}

	// Camera controls
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		camera.setCenter(sf::Vector2f(camera.getCenter().x, camera.getCenter().y - camera_velocity));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		camera.setCenter(sf::Vector2f(camera.getCenter().x, camera.getCenter().y + camera_velocity));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		camera.setCenter(sf::Vector2f(camera.getCenter().x + camera_velocity, camera.getCenter().y));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		camera.setCenter(sf::Vector2f(camera.getCenter().x - camera_velocity, camera.getCenter().y));
	}
}

void Game::run() {
	init();
	sf::Clock clock;
	sf::Time time_last_update = sf::Time::Zero;
	const sf::Time time_per_frame = sf::seconds(1.f / 60.f);
	sf::Time stats_update_time = sf::Time::Zero;
	sf::Clock imgui_clock;

	while (window.isOpen()) {
		sf::Time dt = clock.restart();
		time_last_update += dt;
		ImGui::SFML::Update(window, imgui_clock.restart());
		while (time_last_update > time_per_frame) {
			time_last_update -= time_per_frame;

			// Handle events
			sf::Event e;
			while (window.pollEvent(e)) {
				ImGui::SFML::ProcessEvent(e);
				if (e.type == sf::Event::Closed) {
					window.close();
				}
				else if (e.type == sf::Event::GainedFocus) {
					window_focused = true;
				}
				else if (e.type == sf::Event::LostFocus) {
					paused = true;
					window_focused = false;
				}
			}

			if (window_focused) input();
			update();
		}

		// Update FPS before render and print it
		stats_update_time += dt;
		FPS++;
		if (stats_update_time >= sf::seconds(1.0f)) {
			previous_FPS = FPS;
			stats_update_time -= sf::seconds(1.0f);
			FPS = 0;
		}

		render();
		ImGui::SFML::Render(window);
		window.display();
	}
}

void Game::restartLevel() {
	//    // Clear all entities	(THIS DOESN't ALLOW ENTITIES TO DELETE THEMSELVES)
	//    for (int i=0; i<entities.size()-1; i++) {
	//        delete entities[i];
	//    }
	//    entities.clear();
	//
	for (auto iter = entities.begin(); iter != entities.end(); ) {
		iter = entities.erase(iter);
	}
	entities.clear();
	for (auto c : chunks) {
		for (auto iter = c->entities.begin(); iter != c->entities.end(); ) {
			iter = c->entities.erase(iter);
		}
		c->entities.clear();
	}
	for (auto iter = chunks.begin(); iter != chunks.end(); ) {
		iter = chunks.erase(iter);
	}
	chunks.clear();
	player = NULL;

	// Load the level again
	std::string current_file = current_level->file;
	delete current_level;
	loadLevel(current_file);
}

void Game::loadLevel(std::string file_name) {
	current_level = new SuperFile(file_name);
	current_level->hotload();

	try {
		// Load entities from the SuperFile
		for (auto current_string : current_level->file_contents) {
			std::vector<std::string> results;
			std::stringstream data(current_string);
			std::string current_item;
			while (std::getline(data, current_string, ':')) {
				results.push_back(current_string);
			}

			// Camera max x
			if (results[0] == "camera_x_max") {
				limited_camera_x = true;
				camera_x_max = std::atof(results[1].c_str());
				camera.setCenter(sf::Vector2f(std::atof(results[1].c_str()), camera.getCenter().y));
			}
			// Camera min x
			else if (results[0] == "camera_x_min") {
				limited_camera_x = true;
				camera_x_min = std::atof(results[1].c_str());
				camera.setCenter(sf::Vector2f(std::atof(results[1].c_str()), camera.getCenter().y));
			}
			// Camera max y
			else if (results[0] == "camera_y_max") {
				limited_camera_y = true;
				camera_y_max = std::atof(results[1].c_str());
				camera.setCenter(sf::Vector2f(camera.getCenter().x, std::atof(results[1].c_str())));
			}
			// Camera min y
			else if (results[0] == "camera_y_min") {
				limited_camera_y = true;
				camera_y_min = std::atof(results[1].c_str());
				camera.setCenter(sf::Vector2f(camera.getCenter().x, std::atof(results[1].c_str())));
			}
			// Platform
			else if (results[0] == "platform") {
				entities.push_back(new Platform(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str()))));
			}
			// Flipper
			else if (results[0] == "flipper") {
				entities.push_back(new Flipper(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str()))));
			}
			// Spawn
			else if (results[0] == "spawn") {
				entities.push_back(new Spawn(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str()))));
			}
			// Spike
			else if (results[0] == "spike") {
				entities.push_back(new Spike(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str()))));
			}
			// Walker
			else if (results[0] == "walker") {
				bool left_right = false;
				bool up_side = true;
				try {
					std::istringstream(results[3]) >> std::boolalpha >> left_right;
					std::istringstream(results[4]) >> std::boolalpha >> up_side;
				}
				catch (...) {}
				Walker *walker = new Walker(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str())), left_right);
				walker->up_side = up_side;
				entities.push_back(walker);
			}
			// Door
			else if (results[0] == "door") {
				entities.push_back(new Door(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str())), results[3], true));
			}
			// Switch
			else if (results[0] == "switch") {
				bool on = false;
				std::istringstream(results[3]) >> std::boolalpha >> on;
				entities.push_back(new Switch(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str())), results[3], on));
			}
			// Toggle Platform
			else if (results[0] == "toggle_platform") {
				bool on = false;
				std::istringstream(results[3]) >> std::boolalpha >> on;
				entities.push_back(new TogglePlatform(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str())), results[3], on));
			}
			// EyeBat
			else if (results[0] == "eye_bat") {
				// If there is a set end point                
				if (results.size() == 5 && results[3] != "" && results[4] != "") {
					entities.push_back(
						new EyeBat(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str())),
							sf::Vector2f(std::atof(results[3].c_str()), std::atof(results[4].c_str()))));
				} // Otherwise make it auto_move
				else {
					entities.push_back(
						new EyeBat(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str())),
							sf::Vector2f(NULL, NULL)));
				}
			}
			// Shooter
			else if (results[0] == "shooter") {
				bool left_right = true;
				bool up_side = true;
				std::istringstream(results[3]) >> std::boolalpha >> left_right;
				std::istringstream(results[4]) >> std::boolalpha >> up_side;
				entities.push_back(new Shooter(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str())), left_right, up_side, std::atof(results[5].c_str())));
			}
			// SandBlock
			else if (results[0] == "sand_block") {
				entities.push_back(new SandBlock(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str()))));
			}
			// Chunk
			else if (results[0] == "chunk") {
				chunks.push_back(new Chunk(this, sf::Vector2f(std::atof(results[1].c_str()), std::atof(results[2].c_str())), sf::Vector2f(std::atof(results[3].c_str()), std::atof(results[4].c_str()))));
			}
		}

		// Hard-coded entities
		// ------------------- 

		// Spawn the player and set the player pointer to it
		Player *p = new Player(this);
		for (auto e : entities) {
			if (e->type == "spawn") {
				p->position.x = e->position.x;
				p->position.y = e->position.y - p->size.y;
				break;
			}
		}
		entities.push_back(p);
		player = p;

		// Mattock
		if (current_level->file == "data/caves.map") {
			println("And Jervac said, 'Let there be a mattock!'");
			entities.push_back(new Mattock(this, sf::Vector2f(69 * TILE_SIZE, 3 * TILE_SIZE)));
		}
	}
	catch (...) {
		println("[ERROR]: Failed to load the entities from this level!");
	}

	// Put entities in respective chunks
		for (auto e : entities) {
			for (auto c : chunks) {
				if (e->bounds().intersects(c->bounds())) {
					c->entities.push_back(e);
					// TODO: look into this
					//remove_from_vector(e, entities);
				}
			}
		}

	println(entities.size());
}

void Game::saveLevel() {
	// Clear contents
	current_level->clearFile();

	// Add entities
	for (auto e : entities) {
		// Walker
		if (e->type == "walker") {
			Walker *walker = dynamic_cast<Walker*>(e);
			std::string result = e->type + ":";
			result += std::to_string(floor(e->position.x)) + ":";
			result += std::to_string(floor(e->position.y)) + ":";
			if (walker->left_right) result += "true:";
			else result += "false:";
			if (walker->up_side) result += "true\n";
			else result += "false\n";
			current_level->addString(result);
		}
		// Door
		else if (e->type == "door") {
			Door *door = dynamic_cast<Door*>(e);
			std::string result = e->type + ":";
			result += std::to_string(floor(e->position.x)) + ":";
			result += std::to_string(floor(e->position.y)) + ":";
			result += door->target_level + ":";
			if (door->on_touch) result += "true\n";
			else result += "false\n";
			current_level->addString(result);
		}
		// Switch
		else if (e->type == "switch") {
			Switch *switchy = dynamic_cast<Switch*>(e);
			std::string result = e->type + ":";
			result += std::to_string(floor(e->position.x)) + ":";
			result += std::to_string(floor(e->position.y)) + ":";
			result += switchy->name_tag + ":";
			if (switchy->on) result += "true\n";
			else result += "false\n";
			current_level->addString(result);
		}
		// Toggle Platform
		else if (e->type == "toggle_platform") {
			TogglePlatform *t_platform = dynamic_cast<TogglePlatform*>(e);
			std::string result = e->type + ":";
			result += std::to_string(floor(e->position.x)) + ":";
			result += std::to_string(floor(e->position.y)) + ":";
			result += t_platform->name_tag + ":";
			if (t_platform->on) result += "true\n";
			else result += "false\n";
			current_level->addString(result);
		}
		// EyeBat (x y end_x end_y)
		else if (e->type == "eye_bat") {
			EyeBat *eye_bat = dynamic_cast<EyeBat*>(e);
			std::string result = e->type + ":";
			result += std::to_string(floor(e->position.x)) + ":";
			result += std::to_string(floor(e->position.y));
			if (eye_bat->end_position.x != NULL && eye_bat->end_position.y != NULL) {
				result += ":";
				result += std::to_string(floor(eye_bat->end_position.x)) + ":";
				result += std::to_string(floor(eye_bat->end_position.y));
			}
			result += "\n";
			current_level->addString(result);
		}
		// Shooter (x y left_right up_side shot_delay)
		else if (e->type == "shooter") {
			Shooter *shooter = dynamic_cast<Shooter*>(e);
			std::string result = e->type + ":";
			result += std::to_string(floor(e->position.x)) + ":";
			result += std::to_string(floor(e->position.y)) + ":";
			if (shooter->left_right) result += "true:";
			else result += "false:";
			if (shooter->up_side) result += "true:";
			else result += "false:";
			result += std::to_string(shooter->shot_delay) + "\n";
			current_level->addString(result);
		}
		// SandBlock (x y)
		else if (e->type == "sand_block") {
			SandBlock *sand_block = dynamic_cast<SandBlock*>(e);
			std::string result = e->type + ":";
			result += std::to_string(floor(e->position.x)) + ":";
			result += std::to_string(floor(e->position.y)) + "\n";
			current_level->addString(result);
		}
		// Everything else
		else if (e->type != "player") {
			std::string result = e->type + ":";
			result += std::to_string(floor(e->position.x)) + ":";
			result += std::to_string(floor(e->position.y)) + "\n";
			current_level->addString(result);
		}
	}

	// Save camera boundaries
	if (limited_camera_x) {
		std::string result = "camera_min_x:";
		result += std::to_string(camera_x_min) + "\n";
		current_level->addString(result);

		result = "camera_max_x:";
		result += std::to_string(camera_x_max) + "\n";
		current_level->addString(result);
	}

	if (limited_camera_y) {
		std::string result = "camera_min_y:";
		result += std::to_string(camera_y_min) + "\n";
		current_level->addString(result);

		result = "camera_max_y:";
		result += std::to_string(camera_y_max) + "\n";
		current_level->addString(result);

	}

	// Chunks
	for (auto c : chunks) {
		std::string result = "chunk:";
		result += std::to_string(c->position.x) + ":";
		result += std::to_string(c->position.y) + ":";
		result += std::to_string(c->size.x) + ":";
		result += std::to_string(c->size.y) + "\n";
		current_level->addString(result);
	}
}
#ifndef SUPER_FILE_H
#define SUPER_FILE_H

#include <vector>
#include <string>

struct SuperFile {
    SuperFile(std::string nfile);
    
    bool debug = false;
    std::string file;
    std::vector<std::string> file_contents;
    char delimiter = ':';
    
    // Clear file_contents and load contents of file
    void hotload();
    
    // Returns item at target_index of target_variable's parameters
    std::string getItem(std::string target_variable, int target_index);
    
    // Clears file and file_contents
    void clearFile();
    
    // Adds text to the file and file_contents
    void addString(std::string text);
};
#endif
